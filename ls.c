#include <sys/stat.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <dirent.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

static const char * lookup[] = {"янв", "фев", "мар", "апр", "мая"\
				"июн", "июл", "авг", "сен", "окт", "ноя", "дек"};

void display_contents(char * name){
	struct stat sb;
	struct passwd pswd;
	struct tm * t;
	char link_read[255];
	char * dot = ".";
	ssize_t bytes_read;
	lstat(name, &sb);

	if (strncmp(name, dot, 1) == 0) return;

	printf("%c", S_ISDIR(sb.st_mode)  ? 'd' : \
		     S_ISFIFO(sb.st_mode) ? 'p' : \
		     S_ISLNK(sb.st_mode)  ? 'l' : '-');
		     
	printf("%c%c%c%c%c%c%c%c%c ", 
		(S_IRUSR & sb.st_mode) ? 'r' : '-',
		(S_IWUSR & sb.st_mode) ? 'w' : '-',
		(S_IXUSR & sb.st_mode) ? 'x' : '-',
		(S_IRGRP & sb.st_mode) ? 'r' : '-',
		(S_IWGRP & sb.st_mode) ? 'w' : '-',
		(S_IXGRP & sb.st_mode) ? 'x' : '-',
		(S_IROTH & sb.st_mode) ? 'r' : '-',
		(S_IWOTH & sb.st_mode) ? 'w' : '-',
		(S_IXOTH & sb.st_mode) ? 'x' : '-');
	
	printf("%d\t", sb.st_nlink);
	printf("%s\t%s\t", getpwuid(sb.st_uid)->pw_name,
			   getgrgid(sb.st_gid)->gr_name);
	printf("%5.0lu ", sb.st_size);
	t = localtime(&sb.st_ctime);
	printf("%s ", lookup[t->tm_mon]);
	printf("%02d %02d:%02d ", t->tm_mday, t->tm_hour, t->tm_min);
	if(S_ISLNK(sb.st_mode)){
		printf(ANSI_COLOR_CYAN "%s" ANSI_COLOR_RESET, name);
		link_read[readlink(name, link_read, 254)] = '\0';
		printf(" -> %s\n", link_read);
	}
	else if(S_ISDIR(sb.st_mode))
		printf(ANSI_COLOR_GREEN   "%s" ANSI_COLOR_RESET "\n", name);
	else if((S_IXUSR & sb.st_mode) || (S_IXGRP & sb.st_mode) || (S_IXOTH & sb.st_mode))
		printf(ANSI_COLOR_MAGENTA "%s" ANSI_COLOR_RESET "\n", name);
	else if(S_ISFIFO(sb.st_mode))
		printf(ANSI_COLOR_RED 	  "%s" ANSI_COLOR_RESET "\n", name);
	else 
		printf("%s\n", name);
}	

void get_contents(DIR *d){
	struct dirent *entry;
	int i =0;
	while((entry = readdir(d)) != NULL){
		display_contents(entry->d_name);
	}

}

int ipow(int base, int exp)
{
    int result = 1;
    while (exp)
    {
        if (exp & 1)
            result *= base;
        exp >>= 1;
        base *= base;
    }

    return result;
}

int main(int argc, char *argv[]){

	DIR *d;
	int i=1;
	struct stat s;
	char buf[255];
	if(argc < 2){
		d = opendir(".");
		get_contents(d);
	}
	else {
		for (; argc > 1; argc--){
			lstat(argv[i], &s);
			if(S_ISDIR(s.st_mode)){
				getcwd(buf, sizeof(buf));
				chdir(argv[i]);
				printf("%s\n", argv[i]);
				d = opendir(".");
				get_contents(d);
				chdir(buf);
			}
			else
				display_contents(argv[i]);
			i++;
		}
	}
	
	return 0;
}
